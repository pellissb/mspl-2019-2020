# Groupe
- PELLISSIER Benjamin
- SURIANO Ronny

# Lien du jeu de données
https://www.kaggle.com/mcamli/nba17-18

# Questions proposées
- quelles zones du terrains est la plus interessantes ?
- quel poste est le plus utile ?
- À quel âge un joueur rapporte le plus à son équipe ?

# Question retenue
À quel âge un joueur rapporte le plus à son équipe ?

# Remarques éventuelles
[...]
